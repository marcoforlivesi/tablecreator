// Fill out your copyright notice in the Description page of Project Settings.

#include "TCChairController.h"

#include "TCChair.h"
#include "CornerUtils.h"

#include "Engine/World.h"

// Sets default values for this component's properties
UTCChairController::UTCChairController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UTCChairController::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UTCChairController::SetTarget(ATCObject* target)
{
	this->target = target;
}

void UTCChairController::UpdateChairs()
{
	ATCChair* chair;
	FVector size = target->GetTCSize();
	if (frontChairs.Num() == 0) {
		chair = GetWorld()->SpawnActor<ATCChair>(chairBP);
		frontChairs.Add(chair);

		FVector chairMin;
		chair->GetActorBounds(false, chairMin, chairSize);
		chairSize *= 2;
	}
	else {
		chair = frontChairs.Top();
	}

	int numFrontChairs = GetNumChairs(size.X, chairSize.Y, span.X);
	int numSideChairs = GetNumChairs(size.Y, chairSize.X, span.Y);


	UE_LOG(LogTemp, Warning, TEXT("xDim %s yDim %s"), *FString::SanitizeFloat(size.X), *FString::SanitizeFloat(size.Y));
	UE_LOG(LogTemp, Warning, TEXT("ChairDimX %s ChairDimY %s"), *FString::SanitizeFloat(chairSize.X), *FString::SanitizeFloat(chairSize.Y));
	UE_LOG(LogTemp, Warning, TEXT("frontChairs %s sideChairs %s"), *FString::SanitizeFloat(numFrontChairs), *FString::SanitizeFloat(numSideChairs));

	UpdateChairsNum(numFrontChairs, frontChairs);
	UpdateChairsNum(numFrontChairs, backChairs);
	UpdateChairsNum(numSideChairs, leftChairs);
	UpdateChairsNum(numSideChairs, rightChairs);

	UpdateChairsPosition(frontChairs, ChairType::Front);
	UpdateChairsPosition(backChairs, ChairType::Back);
	UpdateChairsPosition(leftChairs, ChairType::Left);
	UpdateChairsPosition(rightChairs, ChairType::Right);
}

void UTCChairController::UpdateChairsPosition()
{
	UpdateChairsPosition(frontChairs, ChairType::Front);
	UpdateChairsPosition(backChairs, ChairType::Back);
	UpdateChairsPosition(leftChairs, ChairType::Left);
	UpdateChairsPosition(rightChairs, ChairType::Right);
}

// Called every frame
void UTCChairController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

int UTCChairController::GetNumChairs(float tableDim, float chairDim, float span)
{
	return (tableDim - span) / (chairDim + span);
}

void UTCChairController::UpdateChairsNum(const int& desired, TArray<ATCChair*>& chairs)
{
	if (desired > chairs.Num())
	{
		for (int i = chairs.Num(); i < desired; ++i)
		{
			ATCChair* chair = GetWorld()->SpawnActor<ATCChair>(chairBP);
			chairs.Add(chair);
		}
	}
	else
	{
		while(chairs.Num() > desired)
		{
			ATCChair* chair = chairs.Pop(true);
			chair->Destroy();
		}
	}
}

void UTCChairController::UpdateChairsPosition(const TArray<ATCChair*>& chairs, const ChairType& type)
{
	int num = chairs.Num();

	FVector chairStart = target->GetTCLocation();
	FVector size = target->GetTCSize();
	chairStart.Z = 0;
	FQuat rotation;
	switch (type)
	{
	case ChairType::Front:
		chairStart.X -= (chairSize.Y + span.X) * ((int)num / 2) - ((num + 1) % 2) * chairSize.Y * 0.5f;
		chairStart.Y -= size.Y * 0.5;
		rotation = FQuat(FVector::UpVector, PI * 0.5);
		break;
	case ChairType::Back:
		chairStart.X -= (chairSize.Y + span.X) * ((int)num / 2) - ((num + 1) % 2) * chairSize.Y * 0.5f;
		chairStart.Y += size.Y * 0.5;
		rotation = FQuat(FVector::UpVector, PI * -0.5);
		break;
	case ChairType::Left:
		chairStart.Y -= (chairSize.Y + span.Y) * ((int)num / 2) - ((num + 1) % 2) * chairSize.Y * 0.5f;
		chairStart.X -= size.X * 0.5;
		rotation = FQuat(FVector::UpVector, 0);
		break;
	case ChairType::Right:
		chairStart.Y -= (chairSize.Y + span.Y) * ((int)num / 2) - ((num + 1) % 2) * chairSize.Y * 0.5f;
		chairStart.X += size.X * 0.5;
		rotation = FQuat(FVector::UpVector, PI);
		break;
	default:
		break;
	}

	for (ATCChair* chair : chairs)
	{
		chair->SetActorRotation(rotation);
		chair->SetActorLocation(chairStart);

		if (ChairType::Front == type || ChairType::Back == type)
		{
			chairStart.X += span.X + chairSize.Y;
		}
		else
		{
			chairStart.Y += span.Y + chairSize.Y;
		}
	}
}

