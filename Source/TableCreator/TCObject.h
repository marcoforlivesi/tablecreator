// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TCObject.generated.h"

UCLASS()
class TABLECREATOR_API ATCObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATCObject();

	virtual FVector GetTCLocation() const;
	virtual void SetTCLocation(const FVector& location);
	virtual FVector GetTCSize() const;
	virtual void SetTCSize(const FVector& size);
	virtual FVector GetTCScale() const;
	virtual void SetTCScale(const FVector& scale);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
