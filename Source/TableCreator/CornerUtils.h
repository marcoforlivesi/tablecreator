#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"


#define FRONTLEFT_TAG "Front left"
#define FRONTRIGHT_TAG "Front Right"
#define BACKLEFT_TAG "Back left"
#define BACKRIGHT_TAG "Back Right"

class ATCObject;

class CornerUtils
{
public:
	static FVector GetCornerRelativeLocation(ATCObject* object, const FName& type);
	static FVector GetCornerWorldLocation(ATCObject* object, const FName& type);
};