// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TCChairController.generated.h"

class ATCObject;
class ATCChair;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TABLECREATOR_API UTCChairController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTCChairController();

	void SetTarget(ATCObject* target);
	void UpdateChairs();
	void UpdateChairsPosition();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	TSubclassOf<class ATCChair> chairBP;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	FVector2D span;
protected:
	enum class ChairType {
		Front,
		Back,
		Left,
		Right,
	};

	// Called when the game starts
	virtual void BeginPlay() override;

	ATCObject* target;
	FVector chairSize;

	TArray<ATCChair*> leftChairs;
	TArray<ATCChair*> rightChairs;
	TArray<ATCChair*> frontChairs;
	TArray<ATCChair*> backChairs;

	int GetNumChairs(float tableDim, float chairDim, float span);
	void UpdateChairsNum(const int& desired, TArray<ATCChair*>& chairs);
	void UpdateChairsPosition(const TArray<ATCChair*>& chairs, const ChairType& type);
	
};
