// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TCObject.h"
#include "TCChair.generated.h"

UCLASS()
class TABLECREATOR_API ATCChair : public ATCObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATCChair();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
