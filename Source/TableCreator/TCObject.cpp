// Fill out your copyright notice in the Description page of Project Settings.

#include "TCObject.h"


// Sets default values
ATCObject::ATCObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

FVector ATCObject::GetTCLocation() const
{
	return GetActorLocation();
}

void ATCObject::SetTCLocation(const FVector& location)
{
	SetActorLocation(location);
}

FVector ATCObject::GetTCSize() const
{
	return GetTCScale();
}

void ATCObject::SetTCSize(const FVector& size)
{
	SetTCScale(size);
}

FVector ATCObject::GetTCScale() const
{
	return GetActorScale3D();
}

void ATCObject::SetTCScale(const FVector& scale)
{
	SetActorScale3D(scale);
}

// Called when the game starts or when spawned
void ATCObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATCObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

