// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TCObject.h"
#include "Table.generated.h"

class ATCChair;
class UBoxComponent;
class UTCChairController;

UCLASS()
class TABLECREATOR_API ATable : public ATCObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATable();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void UpdateChairs();

	virtual FVector GetTCLocation() const override;
	virtual void SetTCLocation(const FVector& location) override;
	virtual FVector GetTCSize() const override;
	virtual void SetTCSize(const FVector& size) override;
	virtual FVector GetTCScale() const override;
	virtual void SetTCScale(const FVector& scale) override;
protected:
	enum class ChairType {
		Front,
		Back,
		Left,
		Right,
	};

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void InitSize();

	UFUNCTION()
	void OnHandleChangeEvent();

	UFUNCTION()
	void OnHandleChangeEndEvent();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	UTCChairController* chairController;

	UStaticMeshComponent* top;
	FVector size;
	FVector chairSize;
	void UpdateArmsPosition();
	void UpdateArmPosition(const FName& type);
};
