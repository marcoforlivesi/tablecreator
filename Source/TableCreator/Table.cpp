// Fill out your copyright notice in the Description page of Project Settings.

#include "Table.h"

#include "TCChairController.h"
#include "TCHandle.h"
#include "CornerUtils.h"

#include "Engine/World.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

#define ARM_DISTANCE 0.95f;


// Sets default values
ATable::ATable()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	chairController = CreateDefaultSubobject<UTCChairController>("ChairController");
	chairController->SetTarget(this);
	//chairController->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}


FVector ATable::GetTCLocation() const
{
	return top->GetComponentLocation();
}

void ATable::SetTCLocation(const FVector& location)
{
	top->SetWorldLocation(location);
}

FVector ATable::GetTCSize() const
{
	return size;
}

void ATable::SetTCSize(const FVector& size)
{
	this->size = size;
}

FVector ATable::GetTCScale() const
{
	return top->GetComponentScale();
}

void ATable::SetTCScale(const FVector& scale)
{
	top->SetWorldScale3D(scale);
}

// Called when the game starts or when spawned
void ATable::BeginPlay()
{
	Super::BeginPlay();

	TArray<UActorComponent*> components = GetComponentsByTag(UStaticMeshComponent::StaticClass(), "Top");
	if (components.Num() > 0)
	{
		top = Cast<UStaticMeshComponent>(components.Pop());
	}

	chairController = Cast<UTCChairController>(GetComponentByClass(UTCChairController::StaticClass()));

	TArray<AActor*> handles;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATCHandle::StaticClass(), handles);
	for (int i = 0; i < handles.Num(); ++i)
	{
		ATCHandle* handle = Cast<ATCHandle>(handles[i]);
		handle->SetTarget(this);
		handle->OnHandleChangeSize.AddDynamic(this, &ATable::OnHandleChangeEvent);
		handle->OnHandleChangeEndSize.AddDynamic(this, &ATable::OnHandleChangeEndEvent);
	}

	InitSize();
	chairController->UpdateChairs();
}

void ATable::InitSize()
{
	FVector topMin, topMax;
	top->GetLocalBounds(topMin, topMax);
	FVector topScale = top->GetComponentScale();
	FVector topSize = (topMax - topMin) * topScale;
	UE_LOG(LogTemp, Warning, TEXT("topSize %s %s %s"), *FString::SanitizeFloat(topSize.X), *FString::SanitizeFloat(topSize.Y), *FString::SanitizeFloat(topSize.Z));

	size = topSize;
}

void ATable::OnHandleChangeEvent()
{
	UpdateArmsPosition();
}

void ATable::OnHandleChangeEndEvent()
{
	UpdateChairs();
}



void ATable::UpdateArmsPosition()
{
	UpdateArmPosition(FRONTLEFT_TAG);
	UpdateArmPosition(FRONTRIGHT_TAG);
	UpdateArmPosition(BACKLEFT_TAG);
	UpdateArmPosition(BACKRIGHT_TAG);
}

void ATable::UpdateArmPosition(const FName& type)
{
	TArray<UActorComponent*> arms = GetComponentsByTag(UStaticMeshComponent::StaticClass(), type);
	if (arms.Num() > 0)
	{
		UStaticMeshComponent* arm = Cast<UStaticMeshComponent>(arms.Top());
		FVector location = GetTCLocation();
		FVector origin = arm->GetComponentLocation();
		FVector corner = CornerUtils::GetCornerRelativeLocation(this, type);
		location = location + corner * ARM_DISTANCE;
		location.Z = origin.Z;
		arm->SetWorldLocation(location);
	}
}

// Called every frame
void ATable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	
}

void ATable::UpdateChairs()
{
	chairController->UpdateChairs();
}

