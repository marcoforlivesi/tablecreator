// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TCHandle.generated.h"

class UBoxComponent;
class ATCObject;
class ATable;

UENUM()
enum EHandleType {
	FRONT_LEFT		UMETA(DisplayName = "Front left"),
	FRONT_RIGHT		UMETA(DisplayName = "Front right"),
	BACK_LEFT		UMETA(DisplayName = "Back left"),
	BACK_RIGHT		UMETA(DisplayName = "Back right")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHandleChangeSizeSignature);

UCLASS()
class TABLECREATOR_API ATCHandle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATCHandle();

    UFUNCTION()
    void OnBeginCursorOverEvent(UPrimitiveComponent* TouchedComponent);
    UFUNCTION()
    void OnEndCursorOverEvent(UPrimitiveComponent* TouchedComponent);
    UFUNCTION()
    void OnClickedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);
	UFUNCTION()
	void OnReleasedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonReleased);

	FHandleChangeSizeSignature OnHandleChangeSize;
	FHandleChangeSizeSignature OnHandleChangeEndSize;

	ATCObject* GetTarget() const;
	void SetTarget(ATCObject* target);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	TEnumAsByte<EHandleType> type;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	ATCObject *target;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	FVector2D minSize;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	UBoxComponent* collider;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TableCreator")
	float peekDistance = 500.0f;

	APlayerController* playerController;
	bool startDragging;
	FVector beginTargetSize;
	FVector beginTargetScale;

	FVector startLocation;
	FVector scaleFactor;

	void Init();
	FVector GetInputDirection(const FVector& value);
	virtual FVector GetCorner();
	void LineTrace();
	bool UpdateTargetSize(FVector& newLocation);
	void SetColliderScale(float scale);
	void RestorePosition(const FVector& direction);
};
