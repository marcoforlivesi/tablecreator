
#include "CornerUtils.h"

#include "TCObject.h"



FVector CornerUtils::GetCornerRelativeLocation(ATCObject * object, const FName & type)
{
	FVector targetSize = object->GetTCSize();
	targetSize.Z = 0;

	if (FRONTLEFT_TAG == type)
	{
		targetSize = -targetSize * 0.5f;
	}
	else if (FRONTRIGHT_TAG == type)
	{
		targetSize.X = targetSize.X * 0.5f;
		targetSize.Y = -targetSize.Y * 0.5f;
	}
	else if (BACKLEFT_TAG == type)
	{
		targetSize.X = -targetSize.X * 0.5f;
		targetSize.Y = targetSize.Y * 0.5f;
	}
	else if (BACKRIGHT_TAG == type)
	{
		targetSize = targetSize * 0.5f;
	}
	return targetSize;
}

FVector CornerUtils::GetCornerWorldLocation(ATCObject* object, const FName& type)
{
	FVector targetLocation = object->GetTCLocation();
	FVector targetSize = GetCornerRelativeLocation(object, type);
	FVector corner = targetLocation + targetSize;
	return corner;
}
