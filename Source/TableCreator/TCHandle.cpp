// Fill out your copyright notice in the Description page of Project Settings.

#include "TCHandle.h"

#include "TCObject.h"
#include "Table.h"
#include "CornerUtils.h"

#include "Engine/Classes/Components/BoxComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

#include "DrawDebugHelpers.h"

#define FRONT_TAG "Front"
#define BACK_TAG "Back"
#define LEFT_TAG "Left"
#define RIGHT_TAG "Right"
#define BEGIN_COLLIDER_MULTIPLIER 10.0f
#define END_COLLIDER_MULTIPLIER 0.1f

// Sets default values
ATCHandle::ATCHandle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	startDragging = false;
}

void ATCHandle::OnBeginCursorOverEvent(UPrimitiveComponent * TouchedComponent)
{
	UE_LOG(LogTemp, Warning, TEXT("OnBeginCursorOverEvent %s"), *TouchedComponent->GetName());
}

void ATCHandle::OnEndCursorOverEvent(UPrimitiveComponent * TouchedComponent)
{
	UE_LOG(LogTemp, Warning, TEXT("OnEndCursorOverEvent %s"), *TouchedComponent->GetName());
}


void ATCHandle::OnClickedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	UE_LOG(LogTemp, Warning, TEXT("OnClickedEvent %s"), *TouchedComponent->GetName());
	startDragging = true;
	SetColliderScale(BEGIN_COLLIDER_MULTIPLIER);

	playerController->SetIgnoreLookInput(true);

	startLocation = GetActorLocation();

	FVector mouseLocation, mouseDirection;
	bool found = playerController->DeprojectMousePositionToWorld(mouseLocation, mouseDirection);

	beginTargetSize = target->GetTCSize();
	FVector scale = target->GetTCScale();
	scaleFactor = scale / beginTargetSize;
	scaleFactor.Z = 1;
}

void ATCHandle::OnReleasedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonReleased)
{
	UE_LOG(LogTemp, Warning, TEXT("OnReleasedEvent %s"), *TouchedComponent->GetName());
	startDragging = false;
	SetColliderScale(END_COLLIDER_MULTIPLIER);

	playerController->SetIgnoreLookInput(false);

	OnHandleChangeEndSize.Broadcast();
}



// Called when the game starts or when spawned
void ATCHandle::BeginPlay()
{
	Super::BeginPlay();
	playerController = GetWorld()->GetFirstPlayerController();

	Init();
}


ATCObject* ATCHandle::GetTarget() const
{
	return target;
}

void ATCHandle::SetTarget(ATCObject* target)
{
	this->target = target;
}

// Called every frame
void ATCHandle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (startDragging)
	{
		LineTrace();
		startLocation = GetActorLocation();
		beginTargetSize = target->GetTCSize();
		OnHandleChangeSize.Broadcast();
	}


	if (target)
	{
		FVector corner = GetCorner();
		SetActorLocation(corner);
	}
}

void ATCHandle::Init()
{
	collider = Cast<UBoxComponent>(GetComponentByClass(UBoxComponent::StaticClass()));

	collider->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	collider->OnBeginCursorOver.AddDynamic(this, &ATCHandle::OnBeginCursorOverEvent);
	collider->OnEndCursorOver.AddDynamic(this, &ATCHandle::OnEndCursorOverEvent);
	collider->OnClicked.AddDynamic(this, &ATCHandle::OnClickedEvent);
	collider->OnReleased.AddDynamic(this, &ATCHandle::OnReleasedEvent);

	collider->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

FVector ATCHandle::GetInputDirection(const FVector& value)
{
	FVector ris = value;
	switch (type)
	{
	case EHandleType::FRONT_LEFT:
		ris = -ris;
		break;
	case EHandleType::FRONT_RIGHT:
		ris.Y = -ris.Y;
		break;
	case EHandleType::BACK_LEFT:
		ris.X = -ris.X;
		break;
	case EHandleType::BACK_RIGHT:
		ris = ris;
		break;
	default:
		break;
	}
	return ris;
}

FVector ATCHandle::GetCorner()
{
	FVector targetLocation = target->GetTCLocation();
	FVector corner;
	switch (type)
	{
	case EHandleType::FRONT_LEFT:
		corner = CornerUtils::GetCornerWorldLocation(target, FRONTLEFT_TAG);
		break;
	case EHandleType::FRONT_RIGHT:
		corner = CornerUtils::GetCornerWorldLocation(target, FRONTRIGHT_TAG);
		break;
	case EHandleType::BACK_LEFT:
		corner = CornerUtils::GetCornerWorldLocation(target, BACKLEFT_TAG);
		break;
	case EHandleType::BACK_RIGHT:
		corner = CornerUtils::GetCornerWorldLocation(target, BACKRIGHT_TAG);
		break;
	default:
		break;
	}
	corner.Z = targetLocation.Z;
	return corner;
}

void ATCHandle::LineTrace()
{
	FVector mouseLocation, mouseDirection;
	APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	bool found = playerController->DeprojectMousePositionToWorld(mouseLocation, mouseDirection);
	if (!found)
	{
		UE_LOG(LogTemp, Warning, TEXT("Deproject false"));
		return;
	}

	FVector start = mouseLocation;
	FVector direction = mouseDirection * peekDistance;
	FVector end = direction + start;

	FHitResult hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParam;
	
	found = GetWorld()->LineTraceSingleByChannel(hit, start, end, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParam);
	if (!found)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Hit"));
		return;
	}

	FVector newLocation = hit.Location;
	UpdateTargetSize(newLocation);

	FVector location = GetActorLocation();
	DrawDebugLine(GetWorld(), startLocation, location, FColor::Green, false, 0.1f, 0, 0.1f);
}

bool ATCHandle::UpdateTargetSize(FVector& newLocation)
{
	FVector corner = GetCorner();
	newLocation.Z = startLocation.Z;

	FVector size = target->GetTCSize();
	FVector direction = newLocation - corner;
	FVector sizeDirection = GetInputDirection(direction);
	size = beginTargetSize + sizeDirection;
	if (size.X < minSize.X || size.Y < minSize.Y)
	{
		return false;
	}
	target->SetTCSize(size);
	
	FVector scale = target->GetTCScale();
	FVector newScale = scale + sizeDirection * scaleFactor;
	target->SetTCScale(newScale);

	RestorePosition(direction);

	return true;
}

inline void ATCHandle::SetColliderScale(float scale)
{
	FVector boxScale = collider->GetComponentScale();
	boxScale.X *= scale;
	boxScale.Y *= scale;
	collider->SetWorldScale3D(boxScale);
}

void ATCHandle::RestorePosition(const FVector& direction)
{
	FVector targetLocation = target->GetTCLocation();
	FVector delta = direction * 0.5;
	delta.Z = 0;

	FVector newTargetLocation = targetLocation + delta;
	target->SetTCLocation(newTargetLocation);
}
