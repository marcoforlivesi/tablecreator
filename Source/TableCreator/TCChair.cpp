// Fill out your copyright notice in the Description page of Project Settings.

#include "TCChair.h"




// Sets default values
ATCChair::ATCChair()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATCChair::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATCChair::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

